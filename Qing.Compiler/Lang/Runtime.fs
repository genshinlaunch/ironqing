module Qing.Lang.Runtime

open System
open System.Collections
open System.Collections.Generic
open System.Dynamic
open System.Linq.Expressions
open System.Reflection
open System.Threading.Tasks

open Microsoft.Scripting
open Microsoft.Scripting.Runtime
open Microsoft.Scripting.Utils

open DynamicObjectHelpers
open QingExpression
open RuntimeHelpers
open Util

[<Sealed>]
type TypeModelMetaObject(expression, reflectedType: Type, value) =
    inherit DynamicMetaObject(expression, BindingRestrictions.Empty, value)

    override this.BindGetMember(binder) =
        match reflectedType.GetMember(binder.Name, BindingFlags.Static ||| BindingFlags.Public) with
        | [| mem |] ->
            DynamicMetaObject(
                ensureObjectResult (Expression.MakeMemberAccess(null, mem)),
                this.Restrictions.Merge(BindingRestrictions.GetInstanceRestriction(expression, value))
            )
        | _ -> binder.FallbackGetMember(this)

    override this.BindInvoke(binder, args) =
        if notAllArgsHaveValue args then
            let typeMO = getRuntimeMoFromModel<TypeModel> this
            binder.FallbackInvoke(typeMO, args)
        else
            let constructors = reflectedType.GetConstructors()
            let restrictions = getTargetArgsRestrictions this args true

            match
                [ for c in constructors do
                      if parametersMatchArguments<TypeModel> (c.GetParameters()) args then
                          c ]
            with
            | ctor :: _ ->
                let ctorArgs = convertArguments<TypeModel> args (ctor.GetParameters())
                DynamicMetaObject(ensureObjectResult (Expression.New(ctor, ctorArgs)), restrictions)
            | _ -> createThrow this args restrictions typeof<MissingMemberException> $"无法创建实例"

    override this.BindInvokeMember(binder, args) =
        let members =
            reflectedType.GetMember(binder.Name, BindingFlags.Static ||| BindingFlags.Public)

        match
            [ for mem in members do
                  match mem with
                  | :? MethodInfo as mth when parametersMatchArguments<TypeModel> (mth.GetParameters()) args -> mth
                  | _ -> () ]
        with
        | [] ->
            let typeMO = getRuntimeMoFromModel<TypeModel> this
            binder.FallbackInvokeMember(typeMO, args, null)
        | mth :: _ ->
            let restrictions = getTargetArgsRestrictions this args true
            let callArgs = convertArguments<TypeModel> args (mth.GetParameters())
            DynamicMetaObject(ensureObjectResult (Expression.Call(mth, callArgs)), restrictions)

    override this.BindCreateInstance(binder, args) =
        let constructors = reflectedType.GetConstructors()

        match
            [ for c in constructors do
                  if parametersMatchArguments (c.GetParameters()) args then
                      c ]
        with
        | [] -> binder.FallbackCreateInstance(getRuntimeMoFromModel<TypeModel> this, args)
        | ctor :: _ ->
            let restrictions = getTargetArgsRestrictions this args true
            let ctorArgs = convertArguments<TypeModel> args (ctor.GetParameters())
            DynamicMetaObject(Expression.New(ctor, ctorArgs), restrictions)

and [<Sealed>] TypeModel(reflectedType) =
    member _.ReflectedType = reflectedType

    interface IDynamicMetaObjectProvider with
        member this.GetMetaObject(expression) =
            TypeModelMetaObject(expression, reflectedType, this)

type QingTag =
    { Name: string
      Attrs: Dictionary<string, obj>
      Children: ArrayList }

let rec display (o: obj) =
    match o with
    | null -> "空"
    | :? decimal as d ->
        let str = string d
        if str.Contains('.') then str else str + ".0"
    | :? bool as b -> if b then "真" else "假"
    | :? string as s -> s.Replace("”", "\\”") |> sprintf "“%s”"
    | :? ArrayList as arr -> arr |> Seq.cast |> Seq.map display |> joinStrings "，" |> sprintf "【%s】"
    | :? (byte[]) as bytes -> bytesToHexString bytes
    | :? Task<obj> -> "异步任务：……。"
    | :? ExpandoObject as o ->
        seq { for pair in o -> $"{unmangle pair.Key}：{display pair.Value}" }
        |> joinStrings "，"
        |> sprintf "｛%s｝"
    | :? QingTag as tag ->
        let attrsString =
            seq { for entry in tag.Attrs -> $"{entry.Key}={display entry.Value}" }
            |> joinStrings "，"

        sprintf
            "《%s %s%s》"
            tag.Name
            attrsString
            (if tag.Children.Count = 0 then
                 ""
             else
                 " " + display tag.Children)
    | _ -> string o

[<Sealed>]
type QingFunction
    (
        isAsync: bool,
        parameters: VarOrFunc list,
        defParamKeys: VarOrFunc seq,
        defParamValues: obj seq,
        desc: string,
        body: Delegate
    ) =
    // 自动生成的方法, 用于优化.
    let invokeBody args =
        match args with
        | [] -> (body :?> Func<obj>).Invoke()
        | [ x0 ] -> (body :?> Func<obj, obj>).Invoke(x0)
        | [ x0; x1 ] -> (body :?> Func<obj, obj, obj>).Invoke(x0, x1)
        | [ x0; x1; x2 ] -> (body :?> Func<obj, obj, obj, obj>).Invoke(x0, x1, x2)
        | [ x0; x1; x2; x3 ] -> (body :?> Func<obj, obj, obj, obj, obj>).Invoke(x0, x1, x2, x3)
        | [ x0; x1; x2; x3; x4 ] -> (body :?> Func<obj, obj, obj, obj, obj, obj>).Invoke(x0, x1, x2, x3, x4)
        | [ x0; x1; x2; x3; x4; x5 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5)
        | [ x0; x1; x2; x3; x4; x5; x6 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6)
        | [ x0; x1; x2; x3; x4; x5; x6; x7 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8; x9 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11; x12 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11; x12; x13 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11; x12; x13; x14 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14)
        | [ x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11; x12; x13; x14; x15 ] ->
            (body :?> Func<obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj, obj>)
                .Invoke(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15)
        | _ -> raise Assert.Unreachable

    let defParams = Seq.zip defParamKeys defParamValues |> Seq.toList

    override _.ToString() =
        let paramsString =
            seq {
                for param in parameters -> string param
                for param, def in defParams -> $"{param}={display def}"
            }

        sprintf "@%s【%s】｛%s｝" (if isAsync then "异步" else "") (String.Join("，", paramsString)) desc

    member _.Invoke(posargs, kwargs: IDictionary<string, obj>) =
        let rec buildArgs (parameters: VarOrFunc list) (defParams: (VarOrFunc * _) list) posargs acc =
            match parameters, defParams, posargs with
            | [], [], _ -> List.rev acc
            | [], (param, _) :: defParams, (posarg :: posargs' as posargs) ->
                match kwargs.TryGetValue(param.Mangle()) with
                | true, value -> buildArgs [] defParams posargs (value :: acc)
                | false, _ -> buildArgs [] defParams posargs' (posarg :: acc)
            | [], (name, def) :: defParams, [] ->
                match kwargs.TryGetValue(name.Mangle()) with
                | true, value -> buildArgs [] defParams [] (value :: acc)
                | false, _ -> buildArgs [] defParams [] (def :: acc)
            | target :: parameters, defParams, posargs ->
                match kwargs.TryGetValue(target.Mangle()) with
                | true, value -> buildArgs parameters defParams posargs (value :: acc)
                | false, _ ->
                    match posargs with
                    | posarg :: posargs -> buildArgs parameters defParams posargs (posarg :: acc)
                    | [] -> buildArgs parameters defParams [] (null :: acc)

        let args = buildArgs parameters defParams (List.ofArray posargs) []

        if isAsync then
            let task = new Task<obj>(fun () -> invokeBody args)
            task.Start()
            box task
        else
            invokeBody args

[<Sealed; AbstractClass>]
type QingOps() =
    static member MakeArray(elts: obj[]) = ArrayList(elts)

    static member MakeObject(keys: string[], values: obj[]) =
        let result = ExpandoObject()
        let resultDict = result :> IDictionary<_, _>

        for key, value in Seq.zip keys values do
            resultDict.Add(key, value)

        result

    static member MakeTag(name, attrKeys, attrValues, children: obj[]) =
        { Name = name
          Attrs = buildDict (Seq.zip attrKeys attrValues)
          Children = ArrayList(children) }

    static member Display(o: obj) = display o

    static member Equals(x: obj, y: obj) = x.Equals(y)

    static member GetEnumerator(enumerable: obj) : IEnumerator =
        match enumerable with
        | :? string as s -> (Seq.map string s).GetEnumerator()
        | :? IEnumerable as ie -> ie.GetEnumerator()
        | _ -> invalidOp $"{enumerable.GetType()}不可迭代"

    static member Import(loadedPackages, globals: Scope, sourceUnit: SourceUnit, names) =
        let last = List.last names

        let value =
            match names with
            | [] -> raise Assert.Unreachable
            | [ name ] ->
                if hasMember name loadedPackages then
                    getMember name loadedPackages
                else
                    let filename =
                        System.IO.Path.Combine(System.IO.Path.GetDirectoryName(sourceUnit.Path), name + ".q")

                    let value = sourceUnit.LanguageContext.CreateScope()

                    sourceUnit.LanguageContext.CreateFileUnit(filename).Compile().Run(value)
                    |> ignore

                    setMember last value loadedPackages
                    value
            | _ -> names |> Seq.fold (fun current name -> getMember name current) loadedPackages

        setMember last value globals
        value

    static member Await(task: Task<obj>) =
        task.Wait()
        task.Result

    static member StringSub(left: string, right: string) =
        let lengthDiff = left.Length - right.Length

        let rec loop i =
            if i > lengthDiff then
                left
            else if left[i .. i + right.Length] = right then
                left[..i] + left[i + right.Length ..]
            else
                loop (i + 1)

        loop 0

    static member StringMul(str, multiplier) =
        str |> Seq.replicate multiplier |> joinStrings ""

    static member StringDiv(str: string, divisor) = str[.. str.Length / divisor]
