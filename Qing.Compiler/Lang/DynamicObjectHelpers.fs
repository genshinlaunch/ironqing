module internal Qing.Lang.DynamicObjectHelpers

open System
open System.Collections.Generic
open System.Dynamic
open System.Linq.Expressions
open System.Runtime.CompilerServices

open RuntimeHelpers
open Util

let private sentinel = obj ()

let private getSites = Dictionary<string, CallSite<Func<CallSite, obj, obj>>>()

type private UtilGetMemberBinder(name) =
    inherit GetMemberBinder(name, false)

    override _.FallbackGetMember(target, errorSuggestion) =
        errorSuggestion
        |> ifNull (fun () ->
            DynamicMetaObject(
                Expression.Constant(sentinel),
                target.Restrictions.Merge(BindingRestrictions.GetTypeRestriction(target.Expression, target.LimitType))
            ))

let getMember name o =
    match getSites.TryGetValue(name) with
    | true, site -> site.Target.Invoke(site, o)
    | false, _ ->
        let site = CallSite<_>.Create(UtilGetMemberBinder(name))
        getSites[name] <- site

        site.Target.Invoke(site, o)

let hasMember name o = getMember name o <> sentinel

let private setSites = Dictionary<string, CallSite<Action<CallSite, obj, obj>>>()

type private UtilSetMemberBinder(name) =
    inherit SetMemberBinder(name, false)

    override _.FallbackSetMember(target, value, errorSuggestions) =
        errorSuggestions
        |> ifNull (fun () ->
            createThrow
                target
                null
                BindingRestrictions.Empty
                typeof<MissingMemberException>
                "Members setting not supported")

let setMember name value o =
    match setSites.TryGetValue(name) with
    | true, site -> site.Target.Invoke(site, o, value)
    | false, _ ->
        let site = CallSite<_>.Create(UtilSetMemberBinder(name))
        setSites[name] <- site

        site.Target.Invoke(site, o, value)
