module Qing.Lang.ExpressionTreeGenerator

open System
open System.Collections
open System.Collections.Generic
open System.Dynamic
open System.Linq.Expressions

open Microsoft.Scripting
open Microsoft.Scripting.Utils

open Binders
open QingExpression
open Runtime
open RuntimeHelpers
open Symbols
open Util

type QingModuleLambda = Func<ExpandoObject, Scope, obj>

type private ScopeKind =
    | Loop of loopBreak: LabelTarget * loopContinue: LabelTarget
    | Function of functionReturn: LabelTarget
    | Catch

type private AnalysisScope =
    { Parent: AnalysisScope option
      Names: Dictionary<string, ParameterExpression>
      ScopeKind: ScopeKind }

type private AnalysisState =
    { LoadedPackaged: ParameterExpression
      Globals: ParameterExpression
      SourceUnit: SourceUnit
      Errors: ErrorSink
      // None表示顶级模块.
      Scope: AnalysisScope option }

let private wrapBoolTest expression : Expression =
    let tmp = variable ()

    makeLet
        [ tmp, expression ]
        [ Expression.Condition(
              Expression.TypeIs(tmp, typeof<bool>),
              convert tmp typeof<bool>,
              Expression.Condition(
                  Expression.OrElse(Expression.TypeIs(tmp, typeof<ArrayList>), Expression.TypeIs(tmp, typeof<string>)),
                  Expression.Equal(
                      Expression.Constant(0),
                      Expression.Dynamic(bindGetMember ("Length", false), typeof<int>, tmp)
                  ),
                  Expression.Condition(
                      Expression.Equal(tmp, nullExpr),
                      Expression.Constant(false),
                      Expression.Condition(
                          Expression.TypeIs(tmp, typeof<ValueType>),
                          convert
                              (Expression.Dynamic(
                                  bindBinaryOperation ExpressionType.GreaterThan,
                                  typeof<obj>,
                                  tmp,
                                  Expression.Constant(0)
                              ))
                              typeof<bool>,
                          Expression.Constant(true)
                      )
                  )
              )
          ) ]

let rec private analyzeExpression state expression : Expression =
    match expression.Data with
    | Assign(recursive, target, value) -> analyzeAssign state recursive target value
    | AugAssign(target, op, value) -> analyzeAugAssign state target op value
    | Await value -> analyzeAwait state value
    | Array elts -> analyzeArray state elts
    | Break -> analyzeBreak state expression.SourceSpan
    | BinOp(left, op, right) -> analyzeBinOp state left op right
    | Constant value -> Expression.Constant(value, typeof<obj>) :> Expression
    | Continue -> analyzeContinue state expression.SourceSpan
    | Delegate(parameters, body) -> analyzeDelegate state parameters body
    | For(init, test, step, body) -> analyzeFor state init test step body
    | ForEach(target, iter, body) -> analyzeForEach state target iter body
    | If(test, body, orelse) -> analyzeIf state test body orelse
    | Import names -> analyzeImport state expression.SourceSpan names
    | Lambda(isAsync, parameters, defparams, desc, body) -> analyzeLambda state isAsync parameters defparams desc body
    | Not value -> analyzeNot state value
    | Object entries -> analyzeObject state entries
    | Path path -> analyzePath state path
    | CodeBlock body -> analyzeCodeBlock state body
    | Repeat(times, body) -> analyzeRepeat state times body
    | Return value -> analyzeReturn state expression.SourceSpan value
    | Switch(subject, cases, def) -> analyzeSwitch state subject cases def
    | Tag(name, attrs, children) -> analyzeTag state name attrs children
    | Throw exn -> analyzeThrow state exn
    | Try(body, handler, finalbody) -> analyzeTry state body handler finalbody
    | Until(test, body) -> analyzeUntil state test body
    | While(test, body) -> analyzeWhile state test body
    |> ensureObjectResult

and private analyzeArray state elts =
    Expression.Call(makeArray, Expression.NewArrayInit(typeof<obj>, Seq.map (analyzeExpression state) elts))

and private analyzeObject state (entries: (VarOrFunc * _) seq) =
    Expression.Call(
        makeObject,
        Expression.NewArrayInit(
            typeof<string>,
            entries |> Seq.map (fst >> _.Mangle() >> Expression.Constant) |> Seq.cast
        ),
        Expression.NewArrayInit(typeof<obj>, entries |> Seq.map (snd >> analyzeExpression state))
    )

and private analyzeExprs state expressions : Expression =
    match expressions with
    | [] -> nullExpr
    | [ expr ] -> analyzeExpression state expr
    | _ -> expressions |> Seq.map (analyzeExpression state) |> Seq.toArray |> block []

and private analyzeNot state value =
    Expression.Not(value |> analyzeExpression state |> wrapBoolTest)

and private analyzeBinOp state left op right =
    match op with
    | ExpressionType.AndAlso ->
        Expression.AndAlso(
            left |> analyzeExpression state |> wrapBoolTest,
            analyzeExpression state right |> wrapBoolTest
        )
    | ExpressionType.OrElse ->
        Expression.OrElse(
            left |> analyzeExpression state |> wrapBoolTest,
            analyzeExpression state right |> wrapBoolTest
        )
    | _ ->
        Expression.Dynamic(
            bindBinaryOperation op,
            typeof<obj>,
            analyzeExpression state left,
            analyzeExpression state right
        )

and private analyzeArgs state args =
    let (QingArgs(posargs, kwargs)) = args

    CallInfo(List.length posargs + List.length kwargs, kwargs |> List.map (fst >> _.Mangle())),
    (kwargs |> List.map snd) @ posargs |> List.map (analyzeExpression state)

and private analyzePath state (QingPath(root, segments)) : Expression =
    let name = root.Mangle()

    let rec resolveRoot scope : Expression =
        match scope with
        | None -> Expression.Dynamic(bindGetMember (name, true), typeof<obj>, state.Globals)
        | Some { Parent = parent; Names = names } ->
            match names.TryGetValue(name) with
            | true, var -> var
            | false, _ -> resolveRoot parent

    let exprSoFar = resolveRoot state.Scope

    let folder (exprSoFar: Expression) segment : Expression =
        match segment with
        | VarSegment name -> Expression.Dynamic(bindGetMember (name, false), typeof<obj>, exprSoFar)
        | FuncSegment name -> Expression.Dynamic(bindGetMember ("@" + name, false), typeof<obj>, exprSoFar)
        | IntSegment index ->
            Expression.Dynamic(bindGetIndex (CallInfo(1)), typeof<obj>, [ exprSoFar; Expression.Constant(index) ])
        | ExprSegment expr ->
            Expression.Dynamic(bindGetIndex (CallInfo(1)), typeof<obj>, [ exprSoFar; analyzeExpression state expr ])
        | CallSegment args ->
            let callInfo, args = analyzeArgs state args
            Expression.Dynamic(bindInvoke callInfo, typeof<obj>, exprSoFar :: args)
        | CallMemberSegment(memberName, args) ->
            let callInfo, args = analyzeArgs state args
            Expression.Dynamic(bindInvokeMember (memberName, callInfo), typeof<obj>, exprSoFar :: args)

    Seq.fold folder exprSoFar segments

and private analyzeIf state test body orelse =
    Expression.Condition(
        test |> analyzeExpression state |> wrapBoolTest,
        analyzeExprs state body,
        analyzeExprs state orelse
    )

and private assignGlobal state name right =
    let tmp = variable ()
    makeLet [ tmp, right ] [ Expression.Dynamic(bindSetMember name, typeof<obj>, state.Globals, tmp); tmp ]

and private analyzeAssignSegment state init segment right =
    match segment with
    | VarSegment name -> Expression.Dynamic(bindSetMember name, typeof<obj>, init, right)
    | FuncSegment name -> Expression.Dynamic(bindSetMember ("@" + name), typeof<obj>, init, right)
    | CallMemberSegment _
    | CallSegment _ -> raise Assert.Unreachable
    // TODO: 支持多维索引.
    | ExprSegment expr ->
        Expression.Dynamic(bindSetIndex (CallInfo(1)), typeof<obj>, init, analyzeExpression state expr, right)
    | IntSegment index ->
        Expression.Dynamic(bindSetIndex (CallInfo(1)), typeof<obj>, init, Expression.Constant(index), right)

and private analyzeAssign state recursive target value =
    match target with
    | QingPath(root, []) ->
        let name = root.Mangle()

        match state.Scope with
        | None -> assignGlobal state name (analyzeExpression state value)
        | Some scope ->
            let rec loop scope : Expression =
                match scope.Names.TryGetValue(name) with
                | true, left -> Expression.Assign(left, analyzeExpression state value)
                | _ when recursive ->
                    match scope.Parent with
                    | Some parent -> loop parent
                    | None -> assignGlobal state name (analyzeExpression state value)
                | _ ->
                    let left = variable ()
                    scope.Names.Add(name, left)
                    Expression.Assign(left, analyzeExpression state value)

            loop scope
    | QingPath(root, segments) ->
        let right = analyzeExpression state value
        let init, last = initAndLast segments
        let newPath = QingPath(root, init)

        analyzeAssignSegment state (analyzePath state newPath) last right

and private analyzeAugAssign state target op value =
    match target with
    | QingPath(_, []) ->
        analyzeAssign
            state
            true
            target
            { Data =
                BinOp(
                    { Data = Path target
                      SourceSpan = SourceSpan.None },
                    op,
                    value
                )
              SourceSpan = SourceSpan.None }
    | QingPath(root, segments) ->
        let value = analyzeExpression state value
        let init, last = initAndLast segments
        let newPath = QingPath(root, init)
        let tmpLhs = variable ()

        match last with
        | ExprSegment expr ->
            let tmpIdx = variable ()
            let tmpRhs = variable ()

            makeLet
                [ tmpLhs, analyzePath state newPath
                  tmpIdx, analyzeExpression state expr
                  tmpRhs,
                  Expression.Dynamic(
                      bindBinaryOperation op,
                      typeof<obj>,
                      Expression.Dynamic(bindGetIndex (CallInfo(1)), typeof<obj>, tmpLhs, value)
                  ) ]
                [ Expression.Dynamic(bindSetIndex (CallInfo(1)), typeof<obj>, tmpLhs, tmpIdx, tmpRhs)
                  tmpRhs ]
        | _ ->
            makeLet
                [ tmpLhs, analyzePath state newPath ]
                [ analyzeAssignSegment
                      state
                      tmpLhs
                      last
                      (Expression.Dynamic(bindBinaryOperation op, typeof<obj>, tmpLhs, value)) ]


and private analyzeAwait state value =
    Expression.Call(await, analyzeExpression state value)

and private prepareLoop state =
    let loopBreak = Expression.Label()
    let loopContinue = Expression.Label()

    let innerScope =
        { Parent = state.Scope
          Names = Dictionary()
          ScopeKind = Loop(loopBreak, loopContinue) }

    let innerState = { state with Scope = Some innerScope }

    loopBreak, loopContinue, innerScope, innerState

and private analyzeFor state init test step body =
    // "step"子句在第一次循环时不会执行, 但continue后会执行.
    let firstPass = Expression.Variable(typeof<bool>)
    let loopBreak, loopContinue, innerScope, innerState = prepareLoop state
    let init = analyzeExpression innerState init
    let test = test |> analyzeNot innerState
    let step = analyzeExpression innerState step
    let body = List.map (analyzeExpression innerState) body

    makeLet
        [ firstPass, Expression.Constant(true) ]
        [ Expression.Loop(
              block
                  (innerScope.Names |> Seq.map _.Value)
                  (Expression.IfThenElse(
                      firstPass,
                      block [] [ Expression.Assign(firstPass, Expression.Constant(false)); init ],
                      step
                   )
                   :: Expression.IfThen(test, Expression.Break(loopBreak))
                   :: body),
              loopBreak,
              loopContinue
          ) ]

and private analyzeRepeat state times body =
    let counter = Expression.Variable(typeof<int>)
    let loopBreak, loopContinue, innerScope, innerState = prepareLoop state
    let body = List.map (analyzeExpression innerState) body

    makeLet
        [ counter, convert (analyzeExpression state times) typeof<int> ]
        [ Expression.Loop(
              block
                  (innerScope.Names |> Seq.map _.Value)
                  (Expression.IfThenElse(
                      Expression.LessThanOrEqual(counter, Expression.Constant(0)),
                      Expression.Break(loopBreak),
                      Expression.SubtractAssign(counter, Expression.Constant(1))
                   )
                   :: body),
              loopBreak,
              loopContinue
          ) ]

and private analyzeUntil state test body =
    let loopBreak, loopContinue, innerScope, innerState = prepareLoop state

    Expression.Loop(
        block
            (innerScope.Names |> Seq.map _.Value)
            (seq {
                yield! Seq.map (analyzeExpression innerState) body
                Expression.IfThen(test |> analyzeExpression state |> wrapBoolTest, Expression.Break(loopBreak))
            }),
        loopBreak,
        loopContinue
    )

and private analyzeWhile state test body =
    let loopBreak, loopContinue, innerScope, innerState = prepareLoop state

    Expression.Loop(
        block
            (innerScope.Names |> Seq.map _.Value)
            (Expression.IfThen(analyzeNot state test, Expression.Break(loopBreak))
             :: List.map (analyzeExpression innerState) body),
        loopBreak,
        loopContinue
    )

and private analyzeForEach state target iter body =
    let iter = analyzeExpression state iter
    let iterator = Expression.Variable(typeof<IEnumerator>)
    let targetVar = variable ()
    let loopBreak, loopContinue, innerScope, innerState = prepareLoop state
    innerScope.Names.Add(target.Mangle(), targetVar)
    let body = List.map (analyzeExpression innerState) body

    makeLet
        [ iterator, Expression.Call(getEnumerator, iter) ]
        [ Expression.Loop(
              block
                  (innerScope.Names |> Seq.map _.Value)
                  (Expression.IfThen(Expression.Not(Expression.Call(iterator, moveNext)), Expression.Break(loopBreak))
                   :: Expression.Assign(targetVar, Expression.Property(iterator, current))
                   :: body),
              loopBreak,
              loopContinue
          ) ]

and private analyzeBreak state span =
    match state with
    | { Scope = Some { ScopeKind = Loop(loopBreak, _) } } -> Expression.Break(loopBreak) |> ensureObjectResult
    | _ ->
        state.Errors.Add(state.SourceUnit, "循环外的跳出", span, 10, Severity.Error)
        nullExpr

and private analyzeContinue state span =
    match state with
    | { Scope = Some { ScopeKind = Loop(_, loopContinue) } } -> Expression.Continue(loopContinue) |> ensureObjectResult
    | _ ->
        state.Errors.Add(state.SourceUnit, "循环外的继续", span, 11, Severity.Error)
        nullExpr

and private analyzeReturn state span value =
    match state with
    | { Scope = Some { ScopeKind = Function functionReturn } } ->
        Expression.Return(functionReturn, analyzeExpression state value)
        |> ensureObjectResult
    | _ ->
        state.Errors.Add(state.SourceUnit, "函数外的返回", span, 12, Severity.Error)
        nullExpr

and private analyzeImport state span names =
    match state.Scope with
    | Some _ ->
        state.Errors.Add(state.SourceUnit, "只能在模块顶部导入", span, 13, Severity.Error)
        nullExpr
    | None ->
        Expression.Call(
            import,
            state.LoadedPackaged,
            state.Globals,
            Expression.Constant(state.SourceUnit),
            Expression.Constant(names)
        )

and private analyzeFunctionBody state bindings body =
    let functionReturn = Expression.Label(typeof<obj>)

    let parameters = bindings |> Seq.map fst |> Set

    let innerScope =
        { Parent = state.Scope
          Names = buildDict bindings
          ScopeKind = Function functionReturn }

    let innerState = { state with Scope = Some innerScope }

    // 函数的最后一个表达式默认为返回值.
    let bodyExprs, bodyExprsLast =
        match body with
        | [] -> [], nullExpr
        | _ ->
            let init, last = initAndLast body
            List.map (analyzeExpression innerState) init, analyzeExpression innerState last

    let delegateType =
        Expression.GetFuncType(Array.replicate (List.length bindings + 1) typeof<obj>)

    let body =
        seq {
            yield! bodyExprs
            yield Expression.Label(functionReturn, bodyExprsLast)
        }
        |> block
            [ for pair in innerScope.Names do
                  if parameters |> Set.contains pair.Key |> not then
                      pair.Value ]

    Expression.Lambda(delegateType, body, bindings |> Seq.map snd)

and private analyzeDelegate state parameters body =
    let bindings = [ for param in parameters -> param.Mangle(), variable () ]
    analyzeFunctionBody state bindings body

and private analyzeLambda state isAsync parameters defparams desc body =

    let paramNames =
        (parameters |> List.map _.Mangle())
        @ (defparams |> List.map (fst >> _.Mangle()))

    let paramVars = [ for _ in paramNames -> variable () ]
    let bindings = List.zip paramNames paramVars

    Expression.New(
        makeQingFunction,
        Expression.Constant(isAsync, typeof<bool>),
        Expression.Constant(parameters, typeof<VarOrFunc list>),
        Expression.Constant(defparams |> Seq.map fst, typeof<VarOrFunc seq>),
        Expression.NewArrayInit(typeof<obj>, defparams |> Seq.map (snd >> analyzeExpression state)),
        Expression.Constant(desc |> Option.defaultValue "…"),
        analyzeFunctionBody state bindings body
    )

and private analyzeCodeBlock state body =
    Expression.Lambda<Func<obj>>(analyzeExprs state body)

and private analyzeSwitch state subject cases def =
    Expression.Switch(
        analyzeExpression state subject,
        analyzeExprs state def,
        equals,
        seq {
            for QingCase(value, body) in cases ->
                Expression.SwitchCase(analyzeExprs state body, analyzeExpression state value)
        }
    )

and private analyzeTag state name attrs children =
    Expression.Call(
        makeTag,
        Expression.Constant(name),
        Expression.NewArrayInit(typeof<string>, attrs |> Seq.map (fst >> Expression.Constant) |> Seq.cast),
        Expression.NewArrayInit(typeof<obj>, attrs |> Seq.map (snd >> analyzeExpression state)),
        Expression.NewArrayInit(typeof<obj>, children |> Seq.map (analyzeExpression state))
    )

and private analyzeThrow state exn =
    let tmp = variable ()

    makeLet
        [ tmp, analyzeExpression state exn ]
        [ Expression.Throw(
              Expression.Condition(
                  Expression.TypeIs(tmp, typeof<Exception>),
                  convert tmp typeof<exn>,
                  Expression.New(makeExn, Expression.Call(toString, tmp))
              )
          ) ]

and private analyzeTry state body (QingHandler(target, handlerbody)) finalbody =
    let targetVar = variable ()

    let innerScope =
        { Names = buildDict [ target, targetVar ]
          Parent = state.Scope
          ScopeKind = Catch }

    let innerState = { state with Scope = Some innerScope }

    let body = analyzeExprs state body

    let handlers =
        [| Expression.Catch(targetVar, analyzeExprs innerState handlerbody) |]

    match finalbody with
    | [] -> Expression.TryCatch(body, handlers)
    | _ -> Expression.TryCatchFinally(body, analyzeExprs state finalbody, handlers)

let analyze sourceUnit errors expressions =
    let loadedPackages = Expression.Parameter(typeof<ExpandoObject>)
    let globals = Expression.Parameter(typeof<Scope>)

    let state =
        { LoadedPackaged = loadedPackages
          Globals = globals
          SourceUnit = sourceUnit
          Errors = errors
          Scope = None }

    Expression.Lambda<QingModuleLambda>(analyzeExprs state expressions |> ensureObjectResult, loadedPackages, globals)

let analyzeFileModule sourceUnit errors expressions =
    analyze
        sourceUnit
        errors
        (expressions
         @ [ { Data = Constant(1)
               SourceSpan = SourceSpan.None } ])
