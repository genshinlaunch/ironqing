module Qing.Lang.Hosting

open System
open System.Collections.Generic
open System.Dynamic
open System.IO
open System.Linq.Expressions
open System.Reflection
open System.Runtime.CompilerServices
open System.Text

open Microsoft.Scripting
open Microsoft.Scripting.Runtime
open Microsoft.Scripting.Hosting.Shell
open Microsoft.Scripting.Utils

open DynamicObjectHelpers
open ExpressionTreeGenerator
open Runtime
open Util

type ConsoleErrorSink(writeLine: string * Style -> unit) =
    inherit ErrorSink()

    override this.Add(source, message, span, errorCode, severity) =
        this.Add(message, source.Path, source.GetCode(), source.GetCodeLine(span.Start.Line), span, errorCode, severity)

    override _.Add(message, path, code, line, span, errorCode, severity) =
        match severity with
        | Severity.Ignore -> ()
        | _ ->
            let kind, consoleStyle =
                match severity with
                | Severity.Warning -> "warning", Style.Warning
                | Severity.Error
                | Severity.FatalError -> "error", Style.Error
                | _ -> raise Assert.Unreachable

            let str =
                sprintf "%s(%d, %d) %s %04d: %s" path span.Start.Line span.Start.Column kind errorCode message

            let str =
                if not <| String.IsNullOrWhiteSpace(line) && span.IsValid && span.Length <> 0 then
                    $"{str}{Environment.NewLine}{line}{Environment.NewLine}{String('~', span.Length).PadLeft(Math.Max(0, span.End.Column - 1))}"
                else
                    str

            writeLine (str, consoleStyle)

type QingCompilerOptions() =
    inherit CompilerOptions()

    member val ShowStackTrace = false with get, set
    member val SetIncompleteTokenParseResult = false with get, set

type QingCode(loadedPackages, lambda: Expression<QingModuleLambda>, sourceUnit) =
    inherit ScriptCode(sourceUnit)

    let compiledLambda = lazy lambda.Compile()

    override this.Run() = this.Run(Scope())

    override _.Run(scope) =
        compiledLambda.Value.Invoke(loadedPackages, scope)

type QingContext(manager, options) =
    inherit LanguageContext(manager)

    let assemblies = HashSet<Assembly>()

    let loadedPackages = ExpandoObject()

    let registerAssembly (assembly: Assembly) =
        for tp in assembly.GetExportedTypes() do
            let names = tp.FullName.Split('.')

            let folder table name =
                if hasMember name table then
                    getMember name table :?> ExpandoObject
                else
                    let tmp = ExpandoObject()
                    setMember name tmp table
                    tmp

            seq { for i in 0 .. names.Length - 2 -> names[i] }
            |> Seq.fold folder loadedPackages
            |> setMember names[names.Length - 1] (TypeModel(tp))

    do
        manager.AssemblyLoaded.Add(fun e ->
            if not <| assemblies.Contains(e.Assembly) then
                registerAssembly e.Assembly
                assemblies.Add(e.Assembly) |> ignore)

        manager.GetLoadedAssemblyList() |> Seq.iter registerAssembly

    abstract EvalStdlib: unit -> IDictionary<string, obj>

    default _.EvalStdlib() =
        let result = Dictionary<string, obj>()

        for pair in loadedPackages do
            result.Add(pair.Key, pair.Value)

        let resultScope = Scope(result)

        let executablePath = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0])

        for path in Directory.EnumerateFiles(Path.Combine(executablePath, "Std"), "*.q") do
            // HACK: SourceUnit类构造函数需要一个LanguageContext参数, 而这个函数是QingContext的静态方法. 为了防止无限递归引用, 将context参数设为null.
            // Invoke()中的args1同理.
            // 因此不要在Std的文件中导入其他脚本.
            let provider =
                { new TextContentProvider() with
                    override _.GetReader() =
                        new SourceCodeReader(new StreamReader(path), Encoding.UTF8) }

            let sourceUnit = SourceUnit(null, provider, path, SourceCodeKind.File)
            let source = sourceUnit.GetReader().ReadToEnd()

            let ast = Parser.parse source sourceUnit ErrorSink.Null
            let lambda = analyze sourceUnit ErrorSink.Null ast
            lambda.Compile().Invoke(ExpandoObject(), resultScope) |> ignore

        result

    member private this.Stdlib = lazy this.EvalStdlib()

    member this.AddStdlibToScope(scope: Scope) =
        for entry in this.Stdlib.Value do
            setMember entry.Key entry.Value scope

        scope

    override this.CreateScope() =
        base.CreateScope() |> this.AddStdlibToScope

    override this.CreateScope(dict: IDictionary<_, _>) =
        base.CreateScope(dict) |> this.AddStdlibToScope

    override this.CreateScope(storage: IDynamicMetaObjectProvider) =
        base.CreateScope(storage) |> this.AddStdlibToScope

    override _.CompileSourceCode(sourceUnit, options, errorSink) =
        let counter = ErrorCounter(errorSink)
        let context = CompilerContext(sourceUnit, options, counter)
        let qingOptions = options :?> QingCompilerOptions

        try
            match sourceUnit.Kind with
            | SourceCodeKind.Expression
            | SourceCodeKind.SingleStatement
            | SourceCodeKind.InteractiveCode ->
                let source = sourceUnit.GetReader().ReadToEnd()

                if
                    not qingOptions.SetIncompleteTokenParseResult
                    || BracketMatcher.isComplete source
                then
                    let ast = Parser.parse source sourceUnit context.Errors

                    if counter.AnyError then
                        sourceUnit.CodeProperties <- ScriptCodeParseResult.Invalid
                        null
                    else
                        let lambda = analyze sourceUnit counter ast
                        // 创建QingCode时没有使用manager.Globals.
                        // 如果有需要的话, 加上去.
                        QingCode(loadedPackages, lambda, sourceUnit)
                else
                    sourceUnit.CodeProperties <- ScriptCodeParseResult.IncompleteToken
                    null
            | SourceCodeKind.File
            | SourceCodeKind.AutoDetect
            | SourceCodeKind.Statements ->
                let source = sourceUnit.GetReader().ReadToEnd()
                let ast = Parser.parse source sourceUnit context.Errors

                if counter.AnyError then
                    null
                else
                    let lambda = analyzeFileModule sourceUnit counter ast
                    QingCode(loadedPackages, lambda, sourceUnit)
            | _ -> raise Assert.Unreachable
        with exn ->
            errorSink.Add(sourceUnit, $"Internal error: {exn}", SourceSpan.None, 9999, Severity.FatalError)
            null

    override _.LanguageVersion =
        typeof<QingContext>.Assembly.GetName().Version
        |> ifNull (fun () -> Version(0, 0, 0, 0))

    override _.Options = LanguageOptions(options)

    override _.GetCompilerOptions() =
        QingCompilerOptions(SetIncompleteTokenParseResult = true)

    // 我们需要引入自定义的ErrorSink.
    override this.ExecuteProgram(program) =
        match program.Compile(ConsoleErrorSink(fun (str, _) -> eprintfn "%s" str)) with
        | null -> 1 // 编译错误.
        | scriptCode ->
            match scriptCode.Run() with
            | null -> 0 // 代码返回null.
            | returnValue ->
                let site =
                    CallSite<Func<CallSite, obj, int>>
                        .Create(this.CreateConvertBinder(typeof<int>, true))

                site.Target.Invoke(site, returnValue)
