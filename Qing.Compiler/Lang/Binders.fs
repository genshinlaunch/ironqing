module Qing.Lang.Binders

open System
open System.Dynamic
open System.Linq.Expressions
open System.Reflection

open Microsoft.Scripting
open Microsoft.Scripting.ComInterop

open Runtime
open RuntimeHelpers
open Symbols
open Util

type QingBinaryOperationBinder(operation) =
    inherit BinaryOperationBinder(operation)

    static let getStringOperation expressionType =
        match expressionType with
        | ExpressionType.Add -> stringAdd
        | ExpressionType.Subtract -> stringSub
        | ExpressionType.Multiply -> stringMul
        | ExpressionType.Divide -> stringDiv
        | _ -> invalidOp "不支持的字符串操作"

    override this.FallbackBinaryOperation(target, arg, errorSuggestion) =
        if not target.HasValue || not arg.HasValue then
            this.Defer(target, args = [| arg |])
        else
            let restrictions =
                target.Restrictions
                    .Merge(arg.Restrictions)
                    .Merge(BindingRestrictions.GetTypeRestriction(target.Expression, target.LimitType))
                    .Merge(BindingRestrictions.GetTypeRestriction(arg.Expression, arg.LimitType))

            let left = convert target.Expression target.LimitType
            let right = convert arg.Expression arg.LimitType

            let expression: Expression =
                if operation = ExpressionType.Power then
                    Expression.MakeBinary(operation, convert left typeof<float>, convert right typeof<float>)
                else if target.LimitType = typeof<string> && arg.LimitType = typeof<string> then
                    Expression.Call(getStringOperation operation, left, right)
                else if target.LimitType = arg.LimitType then
                    Expression.MakeBinary(operation, left, right)
                else if operation = ExpressionType.Add && target.LimitType = typeof<string> then
                    Expression.Call(stringAdd, left, Expression.Call(toString, convert arg.Expression typeof<obj>))
                else if operation = ExpressionType.Add && arg.LimitType = typeof<string> then
                    Expression.Call(stringAdd, Expression.Call(toString, convert target.Expression typeof<obj>), right)
                else if target.LimitType = typeof<decimal> && isIntType arg.LimitType then
                    Expression.MakeBinary(operation, left, convert right typeof<decimal>)
                else if isIntType target.LimitType && arg.LimitType = typeof<decimal> then
                    Expression.MakeBinary(operation, convert left typeof<decimal>, right)
                else
                    match
                        numberConversionPriorities.TryGetValue(target.LimitType),
                        numberConversionPriorities.TryGetValue(arg.LimitType)
                    with
                    | (true, targetPriority), (true, argPriority) ->
                        if targetPriority > argPriority then
                            Expression.MakeBinary(operation, left, convert right target.LimitType)
                        else
                            Expression.MakeBinary(operation, convert left arg.LimitType, right)
                    | _ -> Expression.MakeBinary(operation, left, right)

            DynamicMetaObject(ensureObjectResult expression, restrictions)

let bindBinaryOperation = memoize QingBinaryOperationBinder

type QingGetMemberBinder(name, defaultNull) =
    inherit GetMemberBinder(name, true)

    override this.FallbackGetMember(target, errorSuggestion) =
        let comMemberRef = ref null

        if ComBinder.TryBindGetMember(this, target, comMemberRef, true) then
            comMemberRef.Value
        else if not target.HasValue then
            this.Defer(target = target)
        else if isNull target.Value then
            raise <| NullReferenceException()
        else
            match
                target.LimitType.GetMember(name, BindingFlags.Static ||| BindingFlags.Instance ||| BindingFlags.Public)
            with
            | [| mem |] ->
                DynamicMetaObject(
                    ensureObjectResult (Expression.MakeMemberAccess(convert target.Expression mem.DeclaringType, mem)),
                    getTypeRestrictions target
                )
            | _ ->
                errorSuggestion
                |> ifNull (fun () ->
                    if defaultNull then
                        DynamicMetaObject(Expression.Default(typeof<obj>), getTypeRestrictions target)
                    else
                        createThrow
                            target
                            null
                            (getTypeRestrictions target)
                            typeof<MissingMemberException>
                            $"{target.Value}上没有成员\"{name}\"")

let bindGetMember = memoize QingGetMemberBinder

type QingGetIndexBinder(callInfo) =
    inherit GetIndexBinder(callInfo)

    override this.FallbackGetIndex(target, args, errorSuggestion) =
        match ComBinder.TryBindGetIndex(this, target, args) with
        | true, result -> result
        | false, _ ->
            if not target.HasValue || notAllArgsHaveValue args then
                let deferArgs = Array.append [| target |] args
                this.Defer(deferArgs)
            else if isNull target.Value then
                raise <| NullReferenceException()
            else
                let restrictions = getTargetArgsRestrictions target args false

                match args with
                | [| prop |] when target.LimitType = typeof<ExpandoObject> && prop.LimitType = typeof<string> ->
                    let memberExpr =
                        ensureObjectResult (
                            Expression.Call(
                                typeof<QingGetIndexBinder>.GetMethod("GetMember"),
                                convert target.Expression typeof<ExpandoObject>,
                                convert prop.Expression typeof<string>
                            )
                        )

                    DynamicMetaObject(memberExpr, restrictions)
                | _ ->
                    let indexingExpr = ensureObjectResult (getIndexingExpression target args)
                    DynamicMetaObject(indexingExpr, restrictions)

let bindGetIndex = memoize QingGetIndexBinder

type QingInvokeBinder(callInfo) =
    inherit InvokeBinder(callInfo)

    override this.FallbackInvoke(target, args, errorSuggestion) =
        match ComBinder.TryBindInvoke(this, target, args) with
        | true, result -> result
        | false, _ ->
            if not target.HasValue || notAllArgsHaveValue args then
                let deferArgs = Array.append [| target |] args
                this.Defer deferArgs
            else if isNull target.Value then
                raise <| NullReferenceException()
            else if target.LimitType = typeof<QingFunction> then
                let kwargs =
                    Expression.Call(
                        makeObject,
                        Expression.Constant(Array.ofSeq callInfo.ArgumentNames),
                        Expression.NewArrayInit(
                            typeof<obj>,
                            args |> Seq.take callInfo.ArgumentNames.Count |> Seq.map _.Expression
                        )
                    )

                let posargs = args |> Seq.skip callInfo.ArgumentNames.Count |> Seq.map _.Expression

                let expression =
                    Expression.Call(
                        convert target.Expression typeof<QingFunction>,
                        invokeQingFunction,
                        Expression.NewArrayInit(typeof<obj>, posargs),
                        kwargs
                    )

                DynamicMetaObject(ensureObjectResult expression, getTypeRestrictions target)
            else if target.LimitType.IsSubclassOf(typeof<Delegate>) then
                let parameters = target.LimitType.GetMethod("Invoke").GetParameters()

                if parameters.Length <> args.Length then
                    errorSuggestion
                    |> ifNull (fun () ->
                        createThrow
                            target
                            args
                            (getTypeRestrictions target)
                            typeof<InvalidOperationException>
                            $"{target.LimitType}需要{parameters.Length}个参数, 得到了{args.Length}个")
                else
                    let callArgs = convertArguments<TypeModel> args parameters

                    let expression =
                        Expression.Invoke(convert target.Expression target.LimitType, callArgs)

                    DynamicMetaObject(ensureObjectResult expression, getTypeRestrictions target)
            else
                errorSuggestion
                |> ifNull (fun () ->
                    createThrow
                        target
                        args
                        (getTypeRestrictions target)
                        typeof<InvalidOperationException>
                        $"{target.LimitType}不是函数")

let bindInvoke = memoize QingInvokeBinder

type QingInvokeMemberBinder(name, callInfo) =
    inherit InvokeMemberBinder(name, false, callInfo)

    override this.FallbackInvokeMember(target, args, errorSuggestion) =
        match ComBinder.TryBindInvokeMember(this, target, args) with
        | true, result -> result
        | false, _ ->
            if not target.HasValue || notAllArgsHaveValue args then
                let deferArgs = Array.append [| target |] args
                this.Defer(deferArgs)
            else if isNull target.Value then
                raise <| NullReferenceException()
            else if target.LimitType = typeof<ExpandoObject> then
                let restrictions = getTargetArgsRestrictions target args false

                let expression =
                    Expression.Dynamic(
                        bindInvoke callInfo,
                        typeof<obj>,
                        seq {
                            Expression.Dynamic(bindGetMember ("@" + name, false), typeof<obj>, target.Expression)
                            :> Expression

                            yield! args |> Seq.map _.Expression
                        }
                    )

                DynamicMetaObject(expression, restrictions)
            else
                match target.LimitType.GetMember(name, BindingFlags.Instance ||| BindingFlags.Public) with
                | [| :? PropertyInfo | :? FieldInfo |] -> raise <| NotImplementedException("暂不支持调用属性和字段")
                | members ->
                    let restrictions = getTargetArgsRestrictions target args false

                    match
                        [ for mem in members do
                              match mem with
                              | :? MethodInfo as mth when parametersMatchArguments<TypeModel> (mth.GetParameters()) args ->
                                  mth
                              | _ -> () ]
                    with
                    | mth :: _ ->
                        let callArgs = convertArguments<TypeModel> args (mth.GetParameters())

                        DynamicMetaObject(
                            ensureObjectResult (
                                Expression.Call(convert target.Expression target.LimitType, mth, callArgs)
                            ),
                            restrictions
                        )
                    | [] ->
                        errorSuggestion
                        |> ifNull (fun () ->
                            createThrow target args restrictions typeof<MissingMemberException> "找不到调用的成员")

    override this.FallbackInvoke(target, args, errorSuggestion) =
        let argExpressions =
            args |> Array.map _.Expression |> Array.append [| target.Expression |]

        let restrictions = target.Restrictions.Merge(BindingRestrictions.Combine(args))

        DynamicMetaObject(
            Expression.Dynamic(bindInvoke (CallInfo(args.Length)), typeof<obj>, argExpressions),
            restrictions
        )

let bindInvokeMember = memoize QingInvokeMemberBinder

type QingSetMemberBinder(name) =
    inherit SetMemberBinder(name, true)

    override this.FallbackSetMember(target, value, errorSuggestion) =
        match ComBinder.TryBindSetMember(this, target, value) with
        | true, result -> result
        | false, _ ->
            if not target.HasValue then
                this.Defer(target = target)
            else if isNull target.Value then
                raise <| NullReferenceException()
            else
                match
                    target.LimitType.GetMember(
                        name,
                        BindingFlags.Static ||| BindingFlags.Instance ||| BindingFlags.Public
                    )
                with
                | [| mem |] ->
                    let v =
                        match mem.MemberType with
                        | MemberTypes.Property -> Some(convert value.Expression (mem :?> PropertyInfo).PropertyType)
                        | MemberTypes.Field -> Some(convert value.Expression (mem :?> FieldInfo).FieldType)
                        | _ -> None

                    match v with
                    | Some v ->
                        DynamicMetaObject(
                            ensureObjectResult (
                                Expression.Assign(
                                    Expression.MakeMemberAccess(convert target.Expression mem.DeclaringType, mem),
                                    v
                                )
                            ),
                            getTypeRestrictions target
                        )
                    | None ->
                        errorSuggestion
                        |> ifNull (fun () ->
                            createThrow
                                target
                                null
                                (getTypeRestrictions target)
                                typeof<InvalidOperationException>
                                "只能对属性和字段赋值")
                | _ ->
                    errorSuggestion
                    |> ifNull (fun () ->
                        createThrow target null (getTypeRestrictions target) typeof<MissingMemberException> "对象名字冲突")

let bindSetMember = memoize QingSetMemberBinder

type QingSetIndexBinder(callInfo) =
    inherit SetIndexBinder(callInfo)

    override this.FallbackSetIndex(target, indexes, value, errorSuggestion) =
        match ComBinder.TryBindSetIndex(this, target, indexes, value) with
        | true, result -> result
        | false, _ ->
            if not target.HasValue || not value.HasValue || notAllArgsHaveValue indexes then
                let deferArgs = Array.concat [ [| target |]; indexes; [| value |] ]
                this.Defer(deferArgs)
            else if isNull target.Value then
                raise <| NullReferenceException()
            else
                let valueExpr =
                    if value.LimitType = typeof<TypeModel> then
                        (getRuntimeMoFromModel<TypeModel> value).Expression
                    else
                        value.Expression

                let indexingExpr = getIndexingExpression target indexes
                let setIndexExpr = Expression.Assign(indexingExpr, valueExpr)
                let restrictions = getTargetArgsRestrictions target indexes false
                DynamicMetaObject(ensureObjectResult setIndexExpr, restrictions)

let bindSetIndex = memoize QingSetIndexBinder

type QingUnaryOperationBinder(operation) =
    inherit UnaryOperationBinder(operation)

    override this.FallbackUnaryOperation(target, errorSuggestion) =
        if target.HasValue then
            DynamicMetaObject(
                ensureObjectResult (
                    Expression.MakeUnary(operation, convert target.Expression target.LimitType, target.LimitType)
                ),
                target.Restrictions.Merge(getTypeRestrictions target)
            )
        else
            this.Defer(target = target)

let bindUnaryOperation = memoize QingUnaryOperationBinder
