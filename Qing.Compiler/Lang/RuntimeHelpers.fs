module internal Qing.Lang.RuntimeHelpers

open System
open System.Diagnostics
open System.Dynamic
open System.Linq.Expressions
open System.Reflection

open Util

// F#不允许向前引用, 因此TypeModel以泛型参数的形式传入.
// 务必在调用时加上类型参数, 否则F#会将其推断为obj.

let parametersMatchArguments<'TypeModel> (parameters: _[]) (args: _[]) =
    let pred (arg: DynamicMetaObject, param: ParameterInfo) =
        let paramType = param.ParameterType

        paramType = typeof<Type> && arg.LimitType = typeof<'TypeModel>
        || paramType.IsAssignableFrom(arg.LimitType)

    parameters.Length = args.Length && parameters |> Seq.zip args |> Seq.forall pred

let getRuntimeMoFromModel<'TypeModel> (typeModel: DynamicMetaObject) =
    Debug.Assert(typeModel.LimitType = typeof<'TypeModel>, "Internal: MO is not a TypeModel")

    DynamicMetaObject(
        Expression.Property(
            convert typeModel.Expression typeof<'TypeModel>,
            typeof<'TypeModel>.GetProperty("ReflectedType")
        ),
        typeModel.Restrictions.Merge(BindingRestrictions.GetTypeRestriction(typeModel.Expression, typeof<'TypeModel>))
    )

let convertArguments<'TypeModel> (args: DynamicMetaObject seq) (ps: ParameterInfo seq) : Expression seq =
    seq {
        for arg, p in Seq.zip args ps do
            let argExpr =
                if arg.LimitType = typeof<'TypeModel> && p.ParameterType = typeof<Type> then
                    (getRuntimeMoFromModel<'TypeModel> arg).Expression
                else
                    arg.Expression

            Expression.Convert(argExpr, p.ParameterType)
    }

let getTargetArgsRestrictions (target: DynamicMetaObject) (args: DynamicMetaObject[]) instanceRestrictionOnTarget =
    let instanceOrTypeRestriction =
        if instanceRestrictionOnTarget then
            BindingRestrictions.GetInstanceRestriction(target.Expression, target.Value)
        else
            BindingRestrictions.GetTypeRestriction(target.Expression, target.LimitType)

    let restrictions =
        target.Restrictions
            .Merge(BindingRestrictions.Combine(args))
            .Merge(instanceOrTypeRestriction)

    seq {
        for arg in args do
            if arg.HasValue && isNull arg.Value then
                BindingRestrictions.GetInstanceRestriction(arg.Expression, null)
            else
                BindingRestrictions.GetTypeRestriction(arg.Expression, arg.LimitType)
    }
    |> Seq.fold (fun current r -> r.Merge(current)) restrictions

let getIndexingExpression (target: DynamicMetaObject) (indexes: DynamicMetaObject[]) : Expression =
    Debug.Assert(target.HasValue && target.LimitType <> typeof<Array>)

    let indexExpressions =
        indexes
        |> Seq.map (fun i -> Expression.Convert(i.Expression, i.LimitType))
        |> Seq.cast<Expression>

    let convertTarget = Expression.Convert(target.Expression, target.LimitType)

    if target.LimitType.IsArray then
        Expression.ArrayAccess(convertTarget, indexExpressions)
    else
        let props = target.LimitType.GetProperties()

        match
            props
            |> Array.tryFind (fun p -> parametersMatchArguments (p.GetIndexParameters()) indexes)
        with
        | Some res -> Expression.MakeIndex(convertTarget, res, indexExpressions)
        | None ->
            Expression.Throw(
                Expression.New(
                    typeof<MissingMemberException>.GetConstructor([| typeof<string> |]),

                    Expression.Constant("Can't bind because there is no matching indexer.")
                )
            )

let createThrow
    (target: DynamicMetaObject)
    (args: DynamicMetaObject[])
    (moreTests: BindingRestrictions)
    (exn: Type)
    message
    =
    let constructor = exn.GetConstructor([| typeof<string> |])

    DynamicMetaObject(
        Expression.Throw(Expression.New(constructor, Expression.Constant(message)), typeof<obj>),
        target.Restrictions.Merge(BindingRestrictions.Combine(args)).Merge(moreTests)
    )

let ensureObjectResult (expression: Expression) =
    if not expression.Type.IsValueType then
        expression
    else if expression.Type = typeof<Void> then
        Expression.Block(expression, Expression.Default(typeof<obj>))
    else
        convert expression typeof<obj>

let getTypeRestrictions (target: DynamicMetaObject) =
    BindingRestrictions.GetTypeRestriction(target.Expression, target.LimitType)

let notAllArgsHaveValue (args: DynamicMetaObject[]) =
    args |> Array.exists (fun a -> not a.HasValue)
