module Qing.Lang.QingExpression

open System.Linq.Expressions

open Microsoft.Scripting

type VarOrFunc =
    | Var of name: string
    | Func of name: string

    // 青语言要求区分变量和函数, 以'#'和'@'前缀为区别, 在ToString成员中使用.
    // 但.Net本身并没有这样的机制, 因此'@'前缀用来mangle名字.
    // 总而言之, ToString用于显示(如果需要的话), Mangle用于交互.

    override this.ToString() =
        match this with
        | Var name -> "#" + name
        | Func name -> "@" + name

    member this.Mangle() =
        match this with
        | Var name -> name
        | Func name -> "@" + name

type QingExpressionData =
    | Constant of value: obj
    | Not of value: QingExpression
    | BinOp of left: QingExpression * op: ExpressionType * right: QingExpression
    | Path of path: QingPath
    | Array of elts: QingExpression list
    | Object of entries: (VarOrFunc * QingExpression) list
    | Tag of name: string * attrs: (string * QingExpression) list * children: QingExpression list
    | Lambda of
        isAsync: bool *
        parameters: VarOrFunc list *
        defparams: (VarOrFunc * QingExpression) list *
        desc: string option *
        body: QingExpression list
    | CodeBlock of body: QingExpression list
    | Assign of recursive: bool * target: QingPath * value: QingExpression
    | AugAssign of target: QingPath * op: ExpressionType * value: QingExpression
    | If of test: QingExpression * body: QingExpression list * orelse: QingExpression list
    | For of init: QingExpression * test: QingExpression * step: QingExpression * body: QingExpression list
    | While of test: QingExpression * body: QingExpression list
    | Until of test: QingExpression * body: QingExpression list
    | Repeat of times: QingExpression * body: QingExpression list
    | ForEach of target: VarOrFunc * iter: QingExpression * body: QingExpression list
    | Try of body: QingExpression list * handler: QingHandler * finalbody: QingExpression list
    | Throw of exn: QingExpression
    | Return of value: QingExpression
    | Break
    | Continue
    | Switch of subject: QingExpression * QingCases: QingCase list * def: QingExpression list
    | Await of value: QingExpression

    // IronQing-specific Expressions.
    | Delegate of parameters: VarOrFunc list * body: QingExpression list
    | Import of names: string list

and QingHandler = QingHandler of target: string * body: QingExpression list

and QingCase = QingCase of value: QingExpression * body: QingExpression list

and QingSegment =
    | VarSegment of name: string
    | FuncSegment of name: string
    | IntSegment of index: int
    | ExprSegment of expr: QingExpression
    | CallSegment of args: QingArgs
    | CallMemberSegment of memberName: string * args: QingArgs

and QingArgs = QingArgs of posargs: QingExpression list * kwargs: (VarOrFunc * QingExpression) list

and QingPath = QingPath of root: VarOrFunc * subsequence: QingSegment list

and QingExpression =
    { Data: QingExpressionData
      SourceSpan: SourceSpan }
