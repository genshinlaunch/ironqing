module internal Qing.Lang.Util

open System
open System.Collections.Generic
open System.Linq.Expressions

let initAndLast list =
    match List.rev list with
    | [] -> invalidArg (nameof (list)) "输入列表不可为空"
    | last :: initRev -> List.rev initRev, last

let joinStrings (separator: string) (strings: string seq) = String.Join(separator, strings)

let hexStringToBytes (hexString: string) =
    let hexString = hexString.Replace(" ", "")
    [| for i in 0 .. hexString.Length / 2 -> Convert.ToByte(hexString[i * 2 .. i * 2 + 1], 16) |]

let bytesToHexString (bytes: byte[]) =
    match bytes with
    | null -> ""
    | _ -> joinStrings "" <| seq { for i in bytes -> i.ToString("X2") }

let buildDict entries =
    let result = Dictionary()

    for key, value in entries do
        result.Add(key, value)

    result

let swap (x, y) = y, x

let takeLine (s: string) startat =
    String.Join("", s[startat..] |> Seq.takeWhile ((<>) '\n'))

let stringSkip startat (s: string) = s[startat..]

let inline ifNull ([<InlineIfLambda>] ifNullThunk) value =
    match value with
    | null -> ifNullThunk ()
    | _ -> value

let unmangle (s: string) =
    if s.StartsWith("@") then s else "#" + s

let mangle (s: string) = if s.StartsWith("#") then s[1..] else s

let memoize fn =
    let cache = Dictionary()

    fun x ->
        match cache.TryGetValue(x) with
        | true, result -> result
        | false, _ ->
            let tmp = fn x
            cache.Add(x, tmp)
            tmp

let numberConversionPriorities =
    [ typeof<byte>
      typeof<sbyte>
      typeof<uint16>
      typeof<int16>
      typeof<uint>
      typeof<int>
      typeof<uint64>
      typeof<int64>
      typeof<float32>
      typeof<float> ]
    |> Seq.zip
    |> (|>) (Seq.initInfinite id)
    |> readOnlyDict

let isNumberType tp =
    numberConversionPriorities.ContainsKey(tp)

let isIntType tp =
    isNumberType tp && tp <> typeof<float32> && tp <> typeof<float>

let nullExpr: Expression = Expression.Default(typeof<obj>)

let convert expr tp = Expression.Convert(expr, tp)

let variable () = Expression.Variable(typeof<obj>)

let block (variables: ParameterExpression seq) (expressions: Expression seq) : Expression =
    Expression.Block(variables, expressions)

let makeLet bindings body =
    block
        (bindings |> Seq.map fst)
        (seq {
            for var, value in bindings -> Expression.Assign(var, value)
            yield! body
        })
